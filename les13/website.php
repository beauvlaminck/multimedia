<!DOCTYPE html>

<html lang="nl">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="beau vlaminck">
    <meta name="keywords" content="sectionen, Marsman">
    <meta name="date" content="20180208">
    <link rel="stylesheet" href="css/jef.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />

</head>

<body>
    <header>
        <nav>
                <ol>
                    <li><a href="marsman.html">Marsman</a></li>
                    <li><a href="neolithicum.html">Neoliticum Sarthe</a></li>
                    <li><a href="register.html">register</a></li>
                    <li><a href="website.html">James Bond</a></li>
                </ol>
        </nav>
    </header>

    <span id="span1">Dr. No (1962)</span>
    <span id="span2">From Russia With Love (1963)</span>
    <span id="span3">Goldfinger (1964)</span>
    <span id="span4">Thunderball (1965)</span>
    <span id="span5">You Only Live Twice (1967)</span>
    <span id="span6">On Her Majesty’s Secret Service(1969)</span>
    <span id="span7">Diamonds are forever (1971)</span>
    <span id="span8">Live and let die (1973)</span>
    <span id="span9">The man with the golden gun (1974)</span>
    <span id="span10">The spy who loved my (1977)</span>
    <span id="span11">Moonraker (1979)</span>
    <span id="span12">For your eyes only (1981)</span>
    <span id="span13">Octopussy (1983)</span>
    <span id="span14">A view to a kill (1985)</span>
    <span id="span15">The living daylight (1987)</span>
    <span id="span16">Licence to kill (1989)</span>
    <span id="span17">Goldeneye ( 1995)</span>
    <span id="span18">Tomorrow never dies (1997)</span>
    <span id="span19">The world is not enough (1999)</span>
    <span id="span20">Die another day (2002)</span>
    <span id="span21">Casino royal (2006)</span>
    <span id="span22">Quantum of solace (2008)</span>
    <span id="span23">Skyfall (2012)</span>
    <span id="span24">Spectre (2015)</span>

    <h1> website van beau vlaminck</h1>
    <img src="images/007.jpg" alt="James Bond">
    <aside>

        <nav>
            <ol>
                <li><a href="#span1">Dr. No (1962)</a></li>
                <li><a href="#span2">From Russia With Love (1963)</a></li>
                <li><a href="#span3">Goldfinger (1964)</a></li>
                <li><a href="#span4">Thunderball (1965 )</a></li>
                <li><a href="#span5">You Only Live Twice (1967)</a></li>
                <li><a href="#span6">On Her Majesty’s Secret Service(1969)</a></li>
                <li><a href="#span7">Diamonds are forever (1971)</a></li>
                <li><a href="#span8">Live and let die (1973)</a></li>
                <li><a href="#span9">The man with the golden gun (1974)</a></li>
                <li><a href="#span10">The spy who loved my (1977)</a></li>
                <li><a href="#span11">Moonraker (1979)</a></li>
                <li><a href="#span12">For your eyes only (1981)</a></li>
                <li><a href="#span13">Octopussy (1983)</a></li>
                <li><a href="#span14">A view to a kill (1985)</a></li>
                <li><a href="#span15">The living daylight (1987)</a></li>
                <li><a href="#span16">Licence to kill (1989)</a></li>
                <li><a href="#span17">Goldeneye ( 1995)</a></li>
                <li><a href="#span18">Tomorrow never dies (1997)</a></li>
                <li><a href="#span19">The world is not enough (1999)</a></li>
                <li><a href="#span20">Die another day (2002)</a></li>
                <li><a href="#span21">Casino royal (2006)</a></li>
                <li><a href="#span22">Quantum of solace (2008)</a></li>
                <li><a href="#span23">Skyfall (2012)</a></li>
                <li><a href="#span24">Spectre (2015)</a></li>
            </ol>
        </nav>
    </aside>
    <article>
        <section id="section1">
            <img src="images/dr.jpg" alt="James Bond">
            <h1>Dr.No</h1>
            <p> Bond.. James bond. Met deze woorden leerde de hele wereld de charismatische geheim agent 007 kennen<br /> In deze eerste film van de James Bond serie bestrijdt Agent 007 de mysterieuze DR. No.
                <br/>Een geniale wetenschapper die het Amerikaanse ruimteprogramma wil vernietigen
                <br />.In een gevecht tegen de tijd moet Bond naar Jamaica waar hij de bloedmooie Honey Ryder ontmoet en de booswicht aanvalt in zijn eigen hoofdkwartier.
            </p>
        </section>
        <section id="section2">
            <img src="images/fr.jpg" alt="James Bond">
            <h1>From Russia With Love </h1>
            <p>
                James Bond strijdt tegen de boosaardige organisatie SPECTRE, in een bloedstollende jacht op een Russische decoderingsmachiene.
                <br /> Blofeld de topagent van SPECTRE, wil de dood van DR. No wreken en Bond in de val lokken.
                <br /> Een spannende bootrace, een brutale helikopteraanval en een aparte treinrit op de Orient Express bewijzen dat Agent 007 niet zo makkelijk uit te schakelen is.
            </p>
        </section>

        <section id="section3">
            <img src="images/go.jpg" alt="James Bond">
            <h1>Goldfinger</h1>

            <p>
                Geheim agent 007 staat tegenover een van de beruchtste criminelen allertijden: Auric Goldfinger.
                <br /> En nu moet hij al zijn charme ,zijn gadgets en zijn trefzekerheid aanwenden
                <br /> om deze gevaarlijke misdadiger te stoppen in zijn poging om Fort Knox te overvallen en de wereldeconomie te beheersen.
            </p>
        </section>

        <section id="section4">
            <img src="images/th.jpg" alt="James Bond">
            <h1>Thunderball</h1>

            <p>
                De leden van SPECTRE, de internationale misdaadorganisatie hebben geld nodig
                <br /> en beramen een plan ze kapen een Vulcan bommenwerper van de NATO en dreigen de atoombommen te laten ontploffen tenzij er 100 miljoen pond aan ruwe diamanten wordt betaald.
                <br /> alle 00-agenten worden op de zaak gezet maar alleen James Bond vindt een spoor.
                <br /> Zijn zoektocht naar de onderwater verborgen wapens leidt naar Nassau naar Emilio Lago zijn haaien en zijn voluptueuze maîtresse Domino.
            </p>
        </section>

        <section id="section5">
            <img src="images/yo.jpg" alt="James Bond">
            <h1>You Only Live Twice</h1>

            <p>
                Een Amerikaanse ruimtevaartproject wordt onderbroken als 1 van de capsules wordt gekaapt door een Russisch ruimtevaartvoertuig, althans, dat denken de Amerikanen.
                <br /> Zij dreigen met wraak, maar de Britten denken er anders over. James Bond wordt naar Japan gestuurd,<br /> waar hij ontdekt dat Blofeld de maker is van deze onderscheppingsraketten.
                <br /> Met de hulp van zijn Ninja- collega’s verijdelt hij de plannen van SPECTRE en wordt de wereldvrede weer hersteld ?
            </p>
        </section>

        <section id="section6">
            <img src="images/om.jpg" alt="James Bond">
            <h1>On Her Majesty’s Secret Service</h1>
            <p>
                Agent 007 en de avontuurlijke Tracy Di Vicenzo bundelen hun krachten om de boosaardige SPECTRE organisatie te bestrijden in de verraderlijke Zwitserse Alpen. Ernst Savro Blofeld,
                <br /> de leider van SPECTRE , heeft een gewaagd plan bedacht: de ontwikkeling van een dodelijk virus dat miljoenen mensen kan doden.
                <br /> Dit virus zou verspreid moeten worden door een groep van mooie vrouwen, die immuun worden gemaakt in het instituut voor Physiologisch Onderzoek in de bergen.
                <br /> Lijkt wel een mooi klusje voor Bond die uit te schakelen !
            </p>
        </section>

        <section id="section7">
            <img src="images/da.jpg" alt="James Bond">
            <h1>Diamonds are forever</h1>
            <p>
                Tijdens een onderzoek naar de geheimzinnige activiteiten binnen de diamantenwereld ontdekt 007
                <br /> dat zijn snode tegenstander Blofeld een voorraad aan diamanten aanlegt
                <br /> om deze te gebruiken in zijn dodelijke laser-satelliet. <br /> Met de hulp van de mooie smokkelaarster Tiffani case tracht Bond de gek te stoppen.
                <br /> Het lot van de wereld hangt aan een zijden draadje !

            </p>
        </section>

        <section id="section8">
            <img src="images/la.jpg" alt="James Bond">
            <h1>Live and let die </h1>

            <p>
                Met charme, humor en zelfzekerheid kruipt Roger more voor de eerste keer in de huid van de minzame, wereldwijze en dodelijkste Agent 007.
                <br /> Hij moet zich direct in een allesbeslissende strijd storten tegen een beruchte drugsbaron die ten doel gesteld heeft om Bond uit de weg te ruimen en de wereld te veroveren.
            </p>
        </section>

        <section id="section9">
            <img src="images/tm.jpg" alt="James Bond">
            <h1>The man with the golden gun</h1>
            <p>
                James Bond zou een evenknie hebben gevonden in Francisco Scaramanga een wereldvermaarde moordenaar wiens geliefde wapen een kenmerkend gouden pistool is.
                <br /> Wanneer Scaramanga de omvormer bemachtigt,
                <br /> moet agent 007 het toestel heroveren en confronteert hij de getrainde killer in een bloedstollend duel op leven en dood!
            </p>
        </section>

        <section id="section10">
            <img src="images/tsw.jpg" alt="James Bond">
            <h1>The spy who loved me</h1>
            <p>
                James Bond en de mooie Sovjet agente Anya Amasova werken samen om de vermissing van geallieerde en Russische atoomonderzeeërs te onderzoeken,
                <br /> hierbij dienen ze een dodelijk pad te volgen dat leidt naar een multimiljonair en scheepsmagnaat Karl Stromberg.
                <br /> Al spoedig is de enige hoop van de wereld gevestigd op Bond en Anya,
                <br /> wanneer zij het dodelijke plan, voor een wereldwijd nucleair Armageddon, ontdekken!
            </p>
        </section>

        <section id="section11">
            <img src="images/moo.jpg" alt="James Bond">
            <h1>Moonraker</h1>
            <p>
                Agent 007 verkent de ruimte in deze razend spannend avontuur, dat zich afspeelt in Venetië , rio de Janeiro en de uithoeken van het zonnestelsel.
                <br /> Wanneer Bond de geizeling onderzoekt van een Amerikaanse space shuttle, raken hij en de bloedmooie CIA agent Holly Goodhead in een gevecht gewikkeld met Hugo Drax.
                <br /> Deze is een machtsgeile industrieel, wiens plannen alle leven op aarde dreigen te vernietigen.
            </p>
        </section>

        <section id="section12">
            <img src="images/fy.jpg" alt="James Bond">
            <h1>For your eyes only</h1>

            <p>
                Wanneer een Brits schip vlakbij Griekeland tot zinden wordt gebracht,
                <br /> beginnen de wereldmachten een koortsachtige zoektocht naar een supergeheime lading van het schip het controlesysteem van een nucleaire onderzeeër.
                <br /> Op agent 007 rust de bijna onmogelijke taak om samen met de mysterieuze Melina het wrak als eerste op te sporen en zo een wereldomvattende ramp te voorkomen.
            </p>
        </section>

        <section id="section13">
            <img src="images/oc.jpg" alt="James Bond">
            <h1>Octopussy</h1>

            <p>
                In een van de meest spectaculaire openingsscene ooit bestuurt James Bond het kleinste straalvliegtuig ter wereld terwijl hij wordt achtervolgd door een hittezoekende raket.
                <br /> Agent 007 ontmoet zijn evenknie in Octopussy een mysterieuze schoonheid die betrokken is bij een militair complot om de NATO aan te vallen.
                <br /> Alleen 007 kan dit dodelijke plan stoppen. Zijn spannende missie brengt hem in Cuba, india en op een razende circustrein in Duitsland.
            </p>
        </section>
        <section id="section14">
            <img src="images/av.jpg" alt="James Bond">
            <h1>A view to a kill</h1>
            <p>
                Agent 007 moet het opnemen tegen 2 van de dodelijkste schurken :de superintelligente Max Zorin , <br /> product van een genetisch nazi experiment en zijn handlanger May-Day, heerlijk boosaardig gespeeld door Grace Jones. <br /> Deze 2
                zijn bereid miljoenen mensen te vermoorden om de wereldvoorraadmicrochips te bemachtigen. <br /> Maar Bond laat zich niet afschrikken door de extreme Siberische koude, een adembenemende achtervolging op de Eiffeltoren of een halsbrekend
                ritje op een zeppeling over de Golden Gate bridge in San Francisco.
            </p>
        </section>

        <section id="section15">
            <img src="images/tl.jpg" alt="James Bond">
            <h1>The living daylight</h1>

            <p>
                Timoty Dalton maakt zijn debuut als James Bond in dit avontuur boordevol actie waarin hij in zijn eentje de KGB en naar winst hongerende wapenhandelaren om de tuin moet zien te leiden.
                <br /> Wanneer Agent 007 de uit Rusland vluchtende officier Georgi Koskov probeert te helpen, wordt de Rus ontvoerd.
                <br /> James Bond komt in actie en tijdens zijn speurtocht loopt hij de adembenemende Kara tegen het lijf.
                <br /> Tezamen raken ze verstrikt in een wapencomplot tussen 2 wereldmachten. Hun spannende race vol ongelooflijke achtervolgingen eindigt in een keiharde oorlog in de afghaanse woestijn.
            </p>
        </section>

        <section id="section16">
            <img src="images/lt.jpg" alt="James Bond">
            <h1>Licence to kill</h1>

            <p>
                Wanneer de drugsbaron Franz Sanchez wraak neemt door een goede vriend van James Bond af te persen, neemt agent 007 ontslag uit de Britse Geheime Dienst.
                <br /> Hij begint een persoonlijke speurtocht naar de kille crimineel om hem voor eens en altijd uit te schakelen.
                <br /> Samen met Sanchez’ sexy vriendin Lupe Lamora komt hij erachter dat het netwerk zich verder uitrekt dan dat hij had verwacht.
                <br /> Deze keer vecht Bond niet voor zijn land of voor gerechtigheid … maar uit wraak!
            </p>
        </section>

        <section id="section17">
            <img src="images/gol.jpg" alt="James Bond">
            <h1>Goldeneye</h1>

            <p>
                James Bond is terug in een avontuur dat groter, beter en explosiever is dan ooit te voren.
                <br /> Vol ongelofelijke stunts, glamoureuze locaties, mooie vrouwen en snelle auto’s! <br /> De nieuwe vijand van Bond wil heel de westerse wereld uit de kaart vegen, geholpen door de Russische maffia. <br /> Hij steelt een ultra geheime
                helikopter en het dodelijkste Sovjet ruimtewagen.
                <br /> Alleen Bond, James Bond kan vernietiging voorkomen in deze superspannende race tegen de tijd.
            </p>
        </section>

        <section id="section18">
            <img src="images/tnd1.jpg" alt="James Bond">
            <h1>Tomorrow never dies</h1>
            <p>
                Agent 007 moet voorkomen dat een catastrofale Wereldramp in de koppen van de krant Tommorow verschijnt.
                <br /> iemand speelt de supermachten van de wereld tegen elkaar uit en alleen James Bond kan dit stoppen.
                <br /> Met een sensationele actiescene, waaronder een wilde Motoachtervolging door en over Saigon.
            </p>
        </section>

        <section id="section19">
            <img src="images/tw.jpg" alt="James Bond">
            <h1>The world is not enough</h1>

            <p>
                Als agent 007 de opdracht krijgt om de mooie erfgename van een oliebaron te beschermen wordt hij meegezogen in een super spannend en erg gevaarlijk en erg gevaarlijk avontuur.
                <br /> Zijn tegenstander is dit keer Renard een genadeloze anarchist die immuun is geworden voor pijn en daarom bijna onverslaanbaar is.
                <br /> De spanning , actie, de knappe vrouwen en de sarcastische humor alles vind je terug in deze nieuwste Bond.
            </p>
        </section>

        <section id="section20">
            <img src="images/dad.jpg" alt="James Bond">
            <h1>Die another day </h1>

            <p>
                Als zijn top geheime missie wordt gesaboteerd, wordt Agent 007 gemarteld door de vijand.
                <br /> In de steek gelaten door MI6 en ontdaan van zijn 00-lisentie, is James uit op wraak.
                <br /> Hij gaat de confrontatie aan met een super spion een code agente en een gladde miljardair.
                <br /> Die Another Day zit vol spanning, barst van explosieve special effects en zenuwslopende achtervolgingen.
            </p>
        </section>

        <section id="section21">
            <img src="images/cr.jpg" alt="James Bond">
            <h1>Casino royal</h1>

            <p>
                <strong>Bond is back!</strong> <br /> Daniel Craig maakt in Casino Royal zijn spectaculair debuut als “007” James Bond, de meest sexy en gevaarlijkste agent van de Britse geheime dienst.
                <br /> Bonds eerste “00” opdracht leidt hem tot bij Le Chiffre de bankier van een wereldwijd terroristisch netwerk.
                <br /> Om hem uit te schakelen moet Bond Le Chiffre verslaan in een pokerspel met een extreem hoge inzet.
                <br /> Een medewerkster van het Britse ministerie van financiën, de bloed mooie Vesper Lynd wordt aangesteld om hem te vergezellen en over het geld van de overheid te waken. <br /> Terwijl Bond en Vesper de moordpogingen van Le Chiffre
                weten te overleven .
                <br /> groeit er een wederzijdse aantrekkingskracht tussen hen die het paar in nog gevaarlijke situatie brengt …
            </p>
        </section>

        <section id="section22">
            <img src="images/qo.jpg" alt="James Bond">
            <h1>Quantum of solace</h1>

            <p>
                Verraden door Vesper de vrouw van wie hij hield,
                <br /> vecht 007 tegen de drang om zijn nieuwste opdracht tot een persoonlijke wraakactie te maken.
                <br /> Tijdens zijn zoektocht naar gerechtigheid ontmoet Bond de knappe Camille, die hem naar Dominic Greene leidt.
                <br /> Deze meedogenloze zakenman is een belangrijke kracht achter de mysterieuze Quantum organisatie.
                <br /> Verwikkeld in een web van verraad, bedrog en moord doet Bond er alles aan om Greene te pakken te krijgen
            </p>
        </section>

        <section id="section23">
            <img src="images/sk.jpg" alt="James Bond">
            <h1>Skyfall</h1>

            <p>
                De loyaliteit van Bond aan M komt onder vuur te liggen wanneer er een cyberterrorist MI6 aanvalt en de identiteit van alle agenten over de hele wereld onthult.
                <br /> Agent 007 is de enige die M nog kan vertrouwen hi moet de mysterieuze dader opsporen .
                <br /> Het criminele meesterbrein Silva die nergens voor terug deinst in zijn zoektocht naar wraak
            </p>
        </section>

        <section id="section24">
            <img src="images/sp.jpg" alt="James Bond">
            <h1>Spectre</h1>

            <p>
                Een geheimzinnig bericht uit het verleden stuurt James Bond op een missie naar Mexico City en Rome .
                <br /> waar hij de beeld schone weduwe Lucia Sciarra ontmoet.
                <br /> Hij is aanwezig bij een geheime vergadering en ontdekt daar het bestaan van de organisatie SPECTRE.
                <br /> Ondertussen ligt MI6 onder vuur van hun nieuwe baas Max Denbigh .
                <br /> Hij twijfelt over de relevantie van MI6. In de jacht achter de waarheid van SPECTRE te komen schakelt Bond de hulp in van Moneypenny en Q om hem helpen Madeleine Swan te vinden, <br /> die hem wellicht verder kan helpen.
                <br /> Hoe verder bond bij het hart van SPECTRE komt, hoe meer hij erachter komt dat hij misschien wel meer lijkt op zijn aartsrivaal dan hij van tevoren dacht.
            </p>
        </section>

        Samengesteld door beau vlaminck
        </address>
        </footer>
    </article>
    <footer>
        <img src="images/007.gif" alt="James Bond">
    </footer>
</body>

</html>
